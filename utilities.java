private Predicate<LocalDateTime> isWithinTimePeriod(LocalDateTime currentLocalDateTime)
	{
		return locaDateTime -> Duration.between(locaDateTime, currentLocalDateTime).toMinutes() < timeIntervalToCheckForExceptions;
}